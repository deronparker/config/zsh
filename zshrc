stop_if_unset DERON_ZSH_CORE_DIR
stop_if_unset DERON_APPDATA_DIR

# allows `<dirname>` in place of `cd <dirname>`
setopt autocd

# --- completion
zstyle :compinstall filename "$DERON_ZSH_CORE_DIR/zshrc"

autoload -Uz compinit
compinit

# fix ctrl + arrow key
# https://unix.stackexchange.com/a/140499
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# --- history
deron_appdata_zsh_dir="$DERON_APPDATA_DIR/zsh"
mkdir -p "$deron_appdata_zsh_dir"
export HISTFILE="$deron_appdata_zsh_dir/history"
export HISTSIZE=10000
export SAVEHIST=10000
setopt append_history

bindkey -v
bindkey '^R' history-incremental-search-backward

# allow comments in interactive shells
setopt interactivecomments
