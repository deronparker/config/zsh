These `rc` files can be treated like "subscriptions". Run `./gen-list.sh` and save the output to your `.zshrc`.
Delete the lines not relevant to you to "unsubscribe" from those files.
