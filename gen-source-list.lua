#!/bin/lua
-- package.path = Os.getenv("PWD") .. "/lua/?.lua"

local function noChecks()
    return true
end

---@param varname string
---@return boolean
local function envVarIsSet(varname)
    assert(varname:len() > 0)
    local value = os.getenv(varname)
    if value ~= nil then
        return value:len() > 0
    end
    return false
end

---@type table<string, boolean>
local sourced = {}

local config <const> = {
    sources = {
        { name = "init", check = { cmd = nil, func = noChecks } },
        { name = "zshrc", check = { cmd = nil, func = noChecks } },

        { name = "eza", check = { cmd = "eza -v", func = nil } },
        { name = "personal", check = { cmd = nil, func = noChecks } },
        { name = "git", check = { cmd = "git -v", func = nil } },
        { name = "go", check = { cmd = "go version", func = nil } },
        { name = "home-manager", check = { cmd = "home-manager --version", func = nil } },
        { name = "libreoffice", check = { cmd = "libreoffice --version", func = nil } },
        { name = "one-size-fits-most", check = { cmd = nil, func = noChecks } },
        { name = "ripgrep", check = { cmd = "rg -V", func = nil } },
        { name = "rust", check = { cmd = "rustc -V", func = nil } },
        { name = "ssh", check = { cmd = "ssh -V", func = nil } },
        { name = "starship", check = { cmd = "starship -V", func = nil } },

        {
            name = "clipboard-wayland",
            check = {
                cmd = nil,
                func = function()
                    return envVarIsSet("WAYLAND_DISPLAY")
                end,
            },
        },
        {
            name = "clipboard-dependent",
            check = {
                cmd = nil,
                func = function()
                    return sourced["clipboard-wayland"] == true
                end,
            },
        },

        { name = "cleanup", check = { cmd = nil, func = noChecks } },
    },
}

for _, source in ipairs(config.sources) do
    local entry = {
        filepath = "$HOME/.config/zsh/" .. source.name,
        success = false,
        ---@type string
        info = nil,
    }

    if source.check.cmd ~= nil then
        local _, _, exitcode = os.execute(source.check.cmd .. " > /dev/null 2>&1")
        assert(exitcode ~= nil)
        entry.success = exitcode == 0
    else
        assert(source.check.func, "cmd was nil, expected func instead")
        entry.success = source.check.func()
        assert(type(entry.success) == "boolean")
    end

    local srcline = 'source "' .. entry.filepath .. '"'
    if entry.success then
        sourced[source.name] = true
        print(srcline)
    else
        sourced[source.name] = false
        print("# " .. srcline)
    end
end
